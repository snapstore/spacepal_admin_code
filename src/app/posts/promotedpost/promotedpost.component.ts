import { Component, ViewEncapsulation, ViewContainerRef, AfterViewInit, AfterContentInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppConfig } from "../../app.config";
import { Modal } from 'angular2-modal/plugins/bootstrap';
import { ModalModule } from "ng2-modal";
import { FormGroup, FormBuilder, Validators, AbstractControl, FormControl } from '@angular/forms';
import { PromotedPostService } from './promotedpost.service';
import { PagesComponent } from '../../pages/pages.component';

@Component({
    selector: 'promotedpost',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./promotedpost.component.scss'],
    templateUrl: './promotedpost.component.html',
    providers: [PromotedPostService]
})

export class PromotedPostComponent {
    public count: any;
    public p = 1;
    public totalPages: any;
    public rowsOnPage = 10;
    postData: any = [];
    msg: any = false;
    detailData: any;
    postDetail: any;
    category: any;


    constructor(private _appConfig: AppConfig, private _promotedpostservice: PromotedPostService, private router: Router, public _isAdmin: PagesComponent) { }
    ngOnInit() {
        if (this._isAdmin.isAdmin == false) {
            var role = sessionStorage.getItem('role');
            var roleDt = JSON.parse(role);
            for (var x in roleDt) {
                if (x == 'active-post') {
                    if (roleDt[x] == 0) {
                        this.router.navigate(['error']);
                    } else if (roleDt[x] == 100) {
                        jQuery("#btnreject").hide();
                    } else if (roleDt[x] == 110) {
                        jQuery("#btnreject").hide();
                    }
                }
            }
        }

        this.p = 1;
        this.getPage(this.p);

    }

    getPage(p) {
        this._promotedpostservice.getPromotedPost(p - 1, this.rowsOnPage).subscribe(
            result => {
                if (result.data && result.data.length > 0) {
                    this.count = result.count[0].count;
                    this.msg = false;
                    this.postData = result.data;
                } else {
                    this.postData = [];
                    this.msg = "No data Available";
                }
            }
        )
    }
    gotoUserDatail(user) {
        // console.log("user",user)
        this._promotedpostservice.getUserDetail(user).subscribe(
            result => {
                if (result.code == 200) {
                    this.detailData = result.data;
                    jQuery('#userDetail').modal('show');
                }
            }
        )
    }
    gotoPostDetail(postedBy, postId) {
        this._promotedpostservice.getPostDetails(postId, postedBy).subscribe(
            result => {
                jQuery('#postDetail').modal('show');
                this.postDetail = result.data;
                this.category = result.data[0].category[0].category;
            }
        )

    }
}