import { Injectable } from '@angular/core';
import { HttpModule, Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { Configuration } from '../../app.constant';

@Injectable()
export class PushMarketingService {
    public Url: string;

    constructor(public http: Http, public _config: Configuration) { }

    getAllCampaign() {
        let url = this._config.Server + "admin/allcampaign";
        return this.http.get(url, { headers: this._config.headers }).map(res => res.json());
    }
    getAllTargetedUser(campaignId) {
        let url = this._config.Server + "admin/targereduser?campaignId=" + campaignId;
        return this.http.get(url, { headers: this._config.headers }).map(res => res.json());
    }
    getUserDetail(user) {
        let url = this._config.Server + "userDetail?username=" + user;
        return this.http.get(url, { headers: this._config.headers }).map(res => res.json());
    }
    getAllViews(campaignId) {
        let url = this._config.Server + "admin/campaignview?campaignId=" + campaignId;
        return this.http.get(url, { headers: this._config.headers }).map(res => res.json());
    }
    getcampaignInfo(campaignId) {
        let url = this._config.Server + "campaigninfo?campaignId=" + campaignId;
        return this.http.get(url, { headers: this._config.headers }).map(res => res.json());
    }
    getAllClicker(campaignId){
        let url = this._config.Server + "admin/campaignclick?campaignId=" + campaignId;
        return this.http.get(url, { headers: this._config.headers }).map(res => res.json());
    }
    deleteCampaign(id) {
        let url = this._config.Server + "campaign?campId=" + id;
        return this.http.delete(url, { headers: this._config.headers }).map(res => res.json());
    }

}